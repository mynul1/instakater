<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="app-url" content="">
    <meta name="file-base-url" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="robots" content="index, follow">
    <meta name="description" content="" />
    <meta name="keywords" content="">

    
    <link rel="shortcut icon" href="" type="image/x-icon">
    <link rel="icon" href="" type="image/x-icon">
    
    <title>Insta Kater</title>


    <link href="https://fonts.googleapis.com/css2?family=Lobster&family=Raleway:wght@300;400;500;600&display=swap"
        rel="stylesheet"> <!-- Bootstrap CSS -->
    <link href="{{ static_asset('assets/frontend/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{ static_asset('assets/frontend/css/style.css')}}" rel="stylesheet">
    <link href="{{ static_asset('assets/frontend/css/responsive.css')}}" rel="stylesheet">

    @stack('style')
</head>