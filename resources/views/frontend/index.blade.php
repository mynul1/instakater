@extends('frontend.layouts.app')

@section('content')
<header class="header-section" style="background-image: url({{static_asset('assets/frontend/image/banner.jpg')}})">
    <div class="food_search_area align-middle">
        <div class="food_search_area_title">
            <p>
                Support Small Business <br>
                local On Demand Catering for <br> any occasion
            </p>
                <form action="product.html">
            <div class="search_input d-flex">
                    <input class="form-control me-3" type="text" name="search" placeholder="Delivery Address" required>
                    <button type="submit" class="btn btn-insta-one btn_search">Search</button>
            </div>
                </form>
        </div>
    </div>


    </div>
    <div class="clear"></div>
</header>
<section class="about_us">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>What is Insta Kater</h2>
            </div>
            <div class="col-md-4">
                <div class="icon">
                    <img src="asset/image/icon11.png" alt="on demand catering">
                </div>
                <h4>On Demand Catring</h4>
                <p>
                    We’re an online catering marketplace partnering with local business to provide on demand
                    catering to everybody.

                </p>
            </div>
            <div class="col-md-4">
                <div class="icon">
                    <img src="asset/image/icon2.png" alt="on demand catering">
                </div>
                <h4>Support Small Business</h4>
                <p>
                    We Partner exclusively with small Businesses. Supporting the local community and economy. Enjoy
                    carefully crafted meals made by local culinary experts


                </p>
            </div>
            <div class="col-md-4">
                <div class="icon">
                    <img src="asset/image/icon3.png" alt="on demand catering">
                </div>
                <h4>Simple to use</h4>
                <p>
                    Easy to use ordering system makes ordering for your employees or family a breeze.
                </p>
            </div>
        </div>
    </div>
</section>

@endsection