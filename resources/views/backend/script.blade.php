<script>
	$('.show_confirm').click(function(event) {
        var form =  $(this).closest("form");

        event.preventDefault();

         swal({

			title: `Are you sure?`,
   
			text: "If you delete this, it will be gone forever.",

			icon: "warning",

			buttons: true,

			dangerMode: true,

        }).then((result) => {

			console.log(result);

        if (result) {

            form.submit();

            swal({
            position: 'top-end',
            icon: 'success',
            title: 'Deleted Successfully',

            timer: 1500
            })

        } else {
            
            swal({
            position: 'top-end',
            icon: 'success',
            title: 'Data is Safe.',
            timer: 1500
            })

        }
        });
        });

  </script>