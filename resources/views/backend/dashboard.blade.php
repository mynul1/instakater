@extends('backend.layouts.master')
@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-12 grid-margin">
                    <div class="row">
                        <div class="col-12 col-xl-8 mb-4 mb-xl-0">
                            <h3 class="font-weight-bold">Welcome SuperAdmin (Charmaine)!</h3>
                            <h6 class="font-weight-normal mb-0">Have you done your monthly backup yet? You have <span class="text-primary">3 unread alerts!</span></h6>
                        </div>
                        <div class="col-12 col-xl-4">
                            <div class="justify-content-end d-flex">
                                <div class="dropdown flex-md-grow-1 flex-xl-grow-0">
                                    <button class="btn btn-sm btn-light bg-white dropdown-toggle" type="button" id="dropdownMenuDate2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <i class="mdi mdi-calendar"></i> Today (11 Feb 2022)
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuDate2">
                                        <a class="dropdown-item" href="#">January - March</a>
                                        <a class="dropdown-item" href="#">March - June</a>
                                        <a class="dropdown-item" href="#">June - August</a>
                                        <a class="dropdown-item" href="#">August - November</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 grid-margin transparent">
                <div class="row">
                    <div class="col-md-3 mb-4 stretch-card transparent">
                        <div class="card card-tale">
                            <div class="card-body">
                                <p class="mb-4">Course Enquiries</p>
                                <p class="fs-30 mb-2">4006</p>
                                <p>10.00% (30 days)</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 mb-4 stretch-card transparent">
                        <div class="card card-dark-blue">
                            <div class="card-body">
                                <p class="mb-4">Service Enquiries</p>
                                <p class="fs-30 mb-2">61344</p>
                                <p>22.00% (30 days)</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 mb-4 stretch-card transparent">
                        <div class="card card-light-blue">
                            <div class="card-body">
                                <p class="mb-4">General Enquiries</p>
                                <p class="fs-30 mb-2">34040</p>
                                <p>2.00% (30 days)</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 mb-4 stretch-card transparent">
                        <div class="card card-light-danger">
                            <div class="card-body">
                                <p class="mb-4">Number of Clients</p>
                                <p class="fs-30 mb-2">47033</p>
                                <p>0.22% (30 days)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <p class="card-title mb-0">All Courses</p>
                        <div class="table-responsive">
                            <table class="table table-striped table-borderless">
                                <thead>
                                <tr>
                                    <th>Course Name</th>
                                    <th>Course Fee</th>
                                    <th>Last Updated</th>
                                    <th>Upcoming Schedules</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Social Media for Good</td>
                                    <td class="font-weight-bold">$362</td>
                                    <td>21 Sep 2021</td>
                                    <td class="font-weight-medium"><div class="badge badge-warning">2 Schedules</div></td></td>
                                </tr>
                                <tr>
                                    <td>Social Media Graphic Design</td>
                                    <td class="font-weight-bold">$116</td>
                                    <td>13 Jun 2021</td>
                                    <td class="font-weight-medium"><div class="badge badge-warning">1 Schedule</div></td>
                                </tr>
                                <tr>
                                    <td>Infographics Design for Social Media & Email Marketing</td>
                                    <td class="font-weight-bold">$551</td>
                                    <td>28 Sep 2021</td>
                                    <td class="font-weight-medium"><div class="badge badge-success">8 Schedules</div>
                                </tr>
                                <tr>
                                    <td>Beautify your PowerPoint with Infographics</td>
                                    <td class="font-weight-bold">$523</td>
                                    <td>30 Jun 2021</td>
                                    <td class="font-weight-medium"><div class="badge badge-success">8 Schedules</div>
                                </tr>
                                <tr>
                                    <td>Mobile Photography</td>
                                    <td class="font-weight-bold">$781</td>
                                    <td>01 Nov 2021</td>
                                    <td class="font-weight-medium"><div class="badge badge-danger">0 Schedule</div></td>
                                </tr>
                                <tr>
                                    <td>Mobile Videography</td>
                                    <td class="font-weight-bold">$283</td>
                                    <td>20 Mar 2021</td>
                                    <td class="font-weight-medium"><div class="badge badge-warning">1 Schedule</div></td>
                                </tr>
                                <tr>
                                    <td>Social Media Marketing</td>
                                    <td class="font-weight-bold">$897</td>
                                    <td>26 Oct 2021</td>
                                    <td class="font-weight-medium"><div class="badge badge-warning">1 Schedule</div></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection