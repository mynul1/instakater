<footer class="footer">
    <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2022.  Website CMS Developed by<a href="https://www.skydigitalagency.com/" target="_blank">Sky Digital Agency</a>. All rights reserved.</span>
        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="ti-heart text-danger ml-1"></i></span>
    </div>
    <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Designed for: <a href="https://www.thegooddesigners.com/" target="_blank">The Good Designers Pte Ltd</a></span>
    </div>
</footer>