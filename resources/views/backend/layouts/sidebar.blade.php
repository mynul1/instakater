<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="">
                <i class="icon-grid menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="">
                <i class="icon-grid menu-icon"></i>
                <span class="menu-title">Testimonials</span>
            </a>
        </li>

        <div class="pl-3 nav_saparator mt-3">
            <small>Course Manager <span class="text-danger">Under Develop</span></small>
        </div>

        <li class="nav-item ">
            <a class="nav-link" data-toggle="collapse" href="#course_category"
               aria-expanded="false"
               aria-controls="ui-basic">
                <i class="ti-user mr-3"></i>
                <span class="menu-title">Course Category</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="course_category">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="">Category</a></li>
                </ul>
            </div>
        </li>


    </ul>
</nav>
