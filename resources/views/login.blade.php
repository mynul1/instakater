<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Skydash Admin</title>

   <!-- plugins:css -->
   <link rel="stylesheet" href="{{ static_asset('assets/backend/vendors/feather/feather.css') }}">
   <link rel="stylesheet" href="{{ static_asset('assets/backend/vendors/ti-icons/css/themify-icons.css') }}">
   <link rel="stylesheet" href="{{ static_asset('assets/backend/vendors/css/vendor.bundle.base.css') }}">
   <!-- endinject -->

   <!-- inject:css -->
   <link rel="stylesheet" href="{{ static_asset('assets/backend/css/vertical-layout-light/style.css') }}">
   <!-- endinject -->
   {{-- <link rel="shortcut icon" href="{{ static_asset(get_favicon_icon()) }}" /> --}}
</head>


   <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
              <div class="brand-logo">
                <img src="" alt="logo">
                <h4>Hello! Welcome Back</h4>
              </div>
  
              <form class="pt-3" method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                  <input type="email" class="form-control form-control-lg" name="email" value="{{old('email')}}" placeholder="yourmail@mail.com">
                  @error('email')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <input type="password" name="password" class="form-control form-control-lg" placeholder="Password">
                  @error('password')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
                     <!-- Remember Me -->
            <div class="block mt-4">
                <label for="remember_me" class="inline-flex items-center">
                    <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="remember">
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-primary-600 hover:text-gray-900 ml-4" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif
            </div>
                <div class="mt-3">
                  <button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">SIGN IN</button>
                </div>
        
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>

<script src="{{ static_asset('assets/backend/vendors/js/vendor.bundle.base.js') }}"></script>


<script src="{{ static_asset('assets/backend/js/off-canvas.js') }}"></script>
<script src="{{ static_asset('assets/backend/js/hoverable-collapse.js') }}"></script>
<script src="{{ static_asset('assets/backend/js/template.js') }}"></script>
<script src="{{ static_asset('assets/backend/js/settings.js') }}"></script>
<script src="{{ static_asset('assets/backend/js/todolist.js') }}"></script>

</head>

<body>

</html>
