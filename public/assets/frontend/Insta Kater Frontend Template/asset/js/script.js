$(document).ready(function () {
    $('#basic-addon2').on('click', function () {
        $('#phone').val('')
    })

})

function decrase_value(el) {
    var input = Number($(el).siblings('span').text())
    input--;
    if (input < 1) {
        $(el).siblings('span').html(1)
    } else {
        $(el).siblings('span').html(input)
    }
}

function increse_value(el) {
    var input = Number($(el).siblings('span').text())
    input++;
    $(el).siblings('span').html(input)
}

function remove_item(el) {
    var item = $(el).closest('div.single_shop_sidebar-item').remove()

}

function other_amount() {
    $('.custom_tip').empty().append('<input type="number" placeholder="Custom Tip" min="1" class="tip form-control">')
}