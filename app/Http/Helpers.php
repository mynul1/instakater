<?php


use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

if (!function_exists('static_asset')) {
    /**
     * Generate an asset path for the application.
     *
     * @param string $path
     * @param bool|null $secure
     * @return string
     */
    function static_asset($path, $secure = null)
    {
        return app('url')->asset('public/' . $path, $secure);
    }
}


if (!function_exists('getBaseURL')) {
    /**
     * Return the BaseURL
     * @return [type] [description]
     */
    function getBaseURL()
    {
        $root = '//' . $_SERVER['HTTP_HOST'];
        $root .= str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

        return $root;
    }
}


if (!function_exists('getFileBaseURL')) {
    function getFileBaseURL()
    {
        return getBaseURL() . 'public/';
    }
}


if (!function_exists('get_setting')) {
    function get_setting($key, $default = null, $lang = false)
    {
        $settings = Cache::remember('business_settings', 86400, function () {
            // return BusinessSetting::all();
        });

        if ($lang == false) {
            $setting = $settings->where('type', $key)->first();
        } else {
            $setting = $settings->where('type', $key)->where('lang', $lang)->first();
            $setting = !$setting ? $settings->where('type', $key)->first() : $setting;
        }
        return $setting == null ? $default : $setting->value;
    }
}

if (!function_exists('image_upload')) {
    function image_upload($req, $location)
    {

        if ($req) {
            $random2 = Illuminate\Support\Str::random(3);
            // $thumbnail_name = $random2 . '.' . $req->extension();
            $thumbnail_name = $random2 . '-' . $req->getClientOriginalName();

            // $thumbnail->move(public_path('images/category'), $thumbnail_name);
            $req->storeAs('public/' . $location, $thumbnail_name);
            return 'storage/' . $location  . '/' . $thumbnail_name;
        }
    }
}

if (!function_exists('unlink_image')) {
    function unlink_image($url)
    {
        try {
            unlink(public_path() . '/' . $url);
        } catch (\Throwable $th) {
            // alert()->error('Error', __('Something Wrong Try Again!'));
        }
    }
}

if (!function_exists('get_user_image')) {
    function get_user_image()
    {
        try {
            if (auth()->user()->image != null) {
                return auth()->user()->image;
            } else {
                return 'storage/user_avatar.png';
            }
        } catch (\Throwable $th) {
            alert()->error('Error', __('Something Wrong Try Again!'));
        }
    }
}

// if (!function_exists('get_site_logo')) {
//     function get_site_logo()
//     {
//         try {
//             // $app_settings = \App\Models\AppSetting::first();
//             if ($app_settings != null) {
//                 return $app_settings->logo;
//             } else {
//                 return 'storage/site_logo.png';
//             }
//         } catch (\Throwable $th) {
//             alert()->error('Error', __('Something Wrong Try Again!'));
//         }
//     }
// }

// if (!function_exists('get_favicon_icon')) {
//     function get_favicon_icon()
//     {
//         try {
//             $app_settings = \App\Models\AppSetting::first();
//             if ($app_settings != null) {
//                 return $app_settings->favicon_icon;
//             } else {
//                 return 'storage/favicon.png';
//             }
//         } catch (\Throwable $th) {
//             alert()->error('Error', __('Something Wrong Try Again!'));
//         }
//     }
// }


if (!function_exists('generate_slug')) {
    function generate_slug($data)
    {
        try {
            return preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $data)) . '-' . Str::random(5);
        } catch (\Throwable $th) {
            alert()->error('Error', __('Something Wrong Try Again!'));
        }
    }
}
