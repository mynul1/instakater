<?php

use Illuminate\Support\Facades\Route;
use UxWeb\SweetAlert\SweetAlert;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.index');
});

include_once('bulbul.php');

require __DIR__ . '/auth.php';

Route::group(["prefix" => "dashboard", "as" => "admin.", "middleware" => ['auth']], function () {

    Route::get('/', function () {
        alert()->warning('Warning Message', 'Optional Title');
        toastr()->warning('My name is Inigo Montoya. You killed my father, prepare to die!');
        return view('backend.dashboard');
    })->name('dashboard');
});
